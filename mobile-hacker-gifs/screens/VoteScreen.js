import React, { useEffect, useState } from 'react';
import { StyleSheet, Image, View, FlatList, Alert, Modal } from 'react-native';
import Header from '../components/template/Header'
import { Layout, Text, Card, Icon, Button, Avatar, ListItem, List } from '@ui-kitten/components';
import { useScrollToTop } from '@react-navigation/native';
import { useAuthContext } from '../context/auth'
import base64 from 'react-native-base64'
import api from '../services/api'

export default function VoteScreen() {

  const { signOut, useAuth } = useAuthContext();
  const [lstGifs, setLstGifs] = useState([])
  const [userInfo, setUserInfo] = useState({})
  const [modalVisible, setModalVisible] = useState(false);
  const [UserId, setUserid] = useState(0)
  const [Config, setConfig] = useState({})

  const ref = React.useRef(null);
  useScrollToTop(ref);

  const getGifs = () => {
    setLstGifs([])
    api.get('hacker-gifs-public/').then(response => {
      setLstGifs(response.data)
    })
  }

  const onRefreshList = () => {
    getGifs()
  }

  const getUserInfor = (config, user_id) => {
    api.get('users/',config).then(response => {
      const myUser = response.data.filter(i => i.id == user_id);  
      setUserInfo(myUser[0])
    })
    
  }

  const getUseAuth = () => {
    useAuth().then(response => {
      const access = response
      const config = { headers: { Authorization: `Bearer ${access}` }};
      const token = base64.decode(response.split('.')[1])
      const {user_id}  = JSON.parse(token)

      setUserid(user_id)
      setConfig(config)

      if(user_id){
        getGifs()
        getUserInfor(config, user_id)
      } else {
        signOut()
      }
    })
  }

  const votoUp = (values) => {
    let verificado = false
    
    let votoUPGif = { "users_id": UserId, "users_nome": userInfo.username, "gifs_id": values.id, "votes": "1" }
    api.get('votos/', Config).then(response => { 
      response.data.filter(i => {
        if(i.gifs_id == values.id && i.users_id == UserId){
          verificado = true
          Alert.alert('Notificação', 'Esse gif foi votado anteriormente!')
        }
      })

      if(!verificado){
        api.post('votos/',votoUPGif, Config).then(response => {
          const gifs_id = response.data.gifs_id
          api.get(`hacker-gifs/${gifs_id}/`,Config).then(response1 => {
            let contabilizarVoto = response1.data
            contabilizarVoto.votos_positivos = ++contabilizarVoto.votos_positivos
            api.put(`hacker-gifs/${contabilizarVoto.id}/`, contabilizarVoto, Config).then(response => {
              Alert.alert('Notificação', 'Votação feita com sucesso!',[{ text: 'Ok', onPress: () => getGifs() }])
            })
          })
        })
      }
    }).catch(e => {
      Alert.alert('Notificação', 'Seu tempo de sessão expirou!',[{ text: 'Sair', onPress: () => signOut() }])
    })
  }

  const votoDown = async (values) => {
    let verificado = false
    
    let votoDownGif = { "users_id": UserId, "users_nome": userInfo.username, "gifs_id": values.id, "votes": "-1" }
    api.get('votos/', Config).then(response => { 
      response.data.filter(i => {
        if(i.gifs_id == values.id && i.users_id == UserId){
          verificado = true
          Alert.alert('Notificação', 'Esse gif foi votado anteriormente!')
        }
      })

      if(!verificado){
        api.post('votos/',votoDownGif, Config).then(response => {
          const gifs_id = response.data.gifs_id
          api.get(`hacker-gifs/${gifs_id}/`,Config).then(response1 => {
            let contabilizarVoto = response1.data
            contabilizarVoto.votos_negativos = ++contabilizarVoto.votos_negativos
            api.put(`hacker-gifs/${contabilizarVoto.id}/`, contabilizarVoto, Config).then(response => {
              Alert.alert('Notificação', 'Votação feita com sucesso!',[{ text: 'Ok', onPress: () => getGifs() }])
            })
          })
        })
      }
    }).catch(e => {
      Alert.alert('Notificação', 'Seu tempo de sessão expirou!',[{ text: 'Sair', onPress: () => signOut() }])
    })
  }

  useEffect(() => {
    getUseAuth()
  },[])

    //Icones da votação
    const VoteUpIcon = (props) => (
      <Icon {...props} name='arrow-circle-up-outline'/>
    );
    const VoteDownIcon = (props) => (
      <Icon {...props} name='arrow-circle-down-outline'/>
    );
    const PeopleIcon = (props) => (
      <Icon {...props} name='people-outline'/>
    );

  const styles = StyleSheet.create({
    container: {
      flex: 1
    },
    contentContainer: {
      paddingHorizontal: 8,
      paddingVertical: 4,
    },
    item: {
      marginVertical: 4,
    },
  });

  const renderItemHeader = (headerProps, info) => (
    <View {...headerProps} key={info.index}>
      <Text category='h6'>
      {info.item.nome}
      </Text>
      <Text>
        {info.item.descricao}
      </Text>
    </View>
  );

  // const showEleitores = (values) => {
  //   console.log(values)
  //   setModalVisible(true)
  // }

  const renderItemFooter = (footerProps, info) => (
    <Layout style={{ alignSelf:'center', justifyContent: 'center', flexDirection: 'row' }}>
      <Layout style={{ flex: 1}}>
        <Button accessoryLeft={VoteUpIcon} onPress={() => votoUp(info.item)} status='success' appearance='ghost'/>
      </Layout>
      <Layout style={{ flex: 1 }}>
        <Button accessoryLeft={VoteDownIcon} status='danger' onPress={() => votoDown(info.item)} appearance='ghost'/>
      </Layout>
      {/* <Layout style={{ flex: 1 }}>
        <Button accessoryLeft={PeopleIcon} appearance='ghost'  onPress={() => showEleitores(info.item)}/>
      </Layout> */}
    </Layout>
  );

  const renderItem = (info) => (
    <Card
      key={info.item.id.toString()}
      style={styles.item}
      status='basic'
      header={headerProps => renderItemHeader(headerProps, info)}
      footer={footerProps => renderItemFooter(footerProps, info)}>
      <Image source={{ uri: info.item.gif_url }} style={{width: 270, height: 300}}/>
      <Layout style={{ paddingTop: 10 }}>
        <Text>{info.item.votos_positivos} Votos Positivos</Text>
        <Text>{info.item.votos_negativos} Votos Negativos</Text>
      </Layout>
    </Card>
  );
  
  // const ItemImage = (props) => (
  //   <>
  //   { true ? <Avatar size='giant' source={require('../assets/avatar.png')}/> :
  //   <Avatar size='giant' source={{ uri:'https://api.adorable.io/avatars/285/admin@admin.com'}}/> }
  //   </>
  // );

  // const renderItemEleitores = ({ item, index }) => (
  //   <ListItem
  //    title={`${item.nome} ${index + 1}`}
  //    description={`${item.email} ${index + 1}`}
  //    accessoryLeft={ItemImage}
  //   />
  // )

  // const data = new Array(8).fill({
  //   nome: 'admin',
  //   email: 'admin@admin.com.br',
  // });

  return (
    <>
{/* <Modal visible={modalVisible} animationType="slide">
      <Header />
      <Text style={{ fontWeight: 'bold', fontSize: 20, textAlign: 'center' }}>Pessoas que voltaram nesse Gif 😻</Text>
    <Card>
      <Button onPress={() => setModalVisible(false)}>
        Fechar
      </Button>
    </Card>
      <List
        data={data}
        renderItem={renderItemEleitores}
      />
  </Modal> */}

    <Header />
    <Layout style={{ padding: 15, backgroundColor: '#694fad' }}>
      <Text style={{ color: 'white', fontWeight: 'bold', fontSize: 28, textAlign:'center' }}>Ranking dos gifs</Text>
    </Layout>
    { lstGifs.length != 0 ? <FlatList
      data={lstGifs}
      refreshing={false}
      onRefresh={onRefreshList}
      ref={ref}
      contentContainerStyle={{ alignSelf: 'stretch' }}
      renderItem={renderItem}
      keyExtractor={item => item.id.toString()}
    /> : <Text style={{ textAlign: 'center', fontWeight: 'bold', fontSize: 20 }}>Carregando dados...</Text> }
    </>
  );
}