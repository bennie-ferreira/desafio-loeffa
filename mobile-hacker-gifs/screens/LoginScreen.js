import React, { useState } from 'react'
import { Image,Text ,TouchableHighlight, KeyboardAvoidingView, TouchableWithoutFeedback, Keyboard, View } from 'react-native'
import { Layout, Input, Icon } from '@ui-kitten/components';
import Header from '../components/template/Header'
import { useAuthContext } from '../context/auth'

export default function LoginScreen(){
    const [login, setLogin] = useState({ username: null, password: null })
    const { signIn } = useAuthContext();
    const [secureTextEntry, setSecureTextEntry] = React.useState(true);

  const handlerSignIn = () => {
    signIn(login)
  }

  const renderIcon = (props) => (
    <TouchableWithoutFeedback onPress={toggleSecureEntry}>
      <Icon {...props} name={secureTextEntry ? 'eye-off' : 'eye'}/>
    </TouchableWithoutFeedback>
  );

  const toggleSecureEntry = () => {
    setSecureTextEntry(!secureTextEntry);
  };

  const AlertIcon = (props) => (
    <Icon {...props} name='alert-circle-outline'/>
  );

    return (
        <>
        <Header/>
        <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
        <View>
        <KeyboardAvoidingView behavior='position' enabled>
        <Image source={require('../assets/login.png')} style={{ width: 320, height: 200 }}/>
        <Layout style={{ flexDirection: 'column' }}>
            <Layout style={{padding: 10}}>
               <Input placeholder='Login' onChangeText={(value) => setLogin({ username: value, password: login.password })}/>
            </Layout>
            <Layout style={{padding: 10}}>
               <Input placeholder='Password' accessoryRight={renderIcon} captionIcon={AlertIcon} secureTextEntry={secureTextEntry} onChangeText={nextValue => setValue(nextValue)} onChangeText={(value) =>  setLogin({ username: login.username, password: value }) }/>
            </Layout>
            <Layout style={{padding: 20}}>
                <TouchableHighlight underlayColor="#493778" onPress={handlerSignIn} style={{backgroundColor: '#694fad', borderRadius: 20, padding: 15}}>
                    <Text style={{ textAlign: 'center', color: 'white', fontWeight: 'bold' }} >Entrar</Text>
                </TouchableHighlight>
            </Layout>
        </Layout>
        </KeyboardAvoidingView>
        </View>
        </TouchableWithoutFeedback>
        </>
    )
}