import React, { useEffect, useState } from 'react';
import { Image , TouchableHighlight, StyleSheet, View, FlatList, Alert } from 'react-native';
import Header from '../components/template/Header'
import { Layout, Text, Avatar, Card } from '@ui-kitten/components';
import { MaterialIcons } from '@expo/vector-icons';
import { useScrollToTop } from '@react-navigation/native';
import { useAuthContext } from '../context/auth'
import base64 from 'react-native-base64'
import api from '../services/api'

export default function UserScreen() {

  const { signOut, useAuth } = useAuthContext();
  const [userInfo, setUserInfo] = useState({})
  const [userVotos, setUserVotos] = useState([])
  const [userVotosGifs, setUserVotosGifs] = useState([])
  const [refreshIcon, setRefreshIcon] = useState(false)
  
  const ref = React.useRef(null);
  useScrollToTop(ref);

  const getUserInformation = (user_id, access) => {
    const config = { headers: { Authorization: `Bearer ${access}` }};
    api.get('users/',config).then(response => {
      const myUser = response.data.filter(i => i.id == user_id);  
      setUserInfo(myUser[0])
    })
    .catch(error => {
      Alert.alert('Notificação', 'Seu tempo de sessão expirou!',[{ text: 'Sair', onPress: () => signOut() }])
    })
  }

  const getUserVotos = async (user_id, access) => {
    const  config = { headers: { Authorization: `Bearer ${access}` }};
    setUserVotosGifs([])
    api.get('votos/',config).then(response => setUserVotos(response.data.filter(i => i.users_id == user_id)))
    
    if(userVotos.length != 0){
      userVotos.forEach(i => {
        api.get(`hacker-gifs/${i.gifs_id}/`,config).then(response => {
          setUserVotosGifs(userVotosGifs => [...userVotosGifs, response.data])
        })
      })
    }

  }

  const getUseAuth = () => {
    useAuth().then(response => {
      const access = response
      const token = base64.decode(response.split('.')[1])
      const {user_id}  = JSON.parse(token)
      if(user_id){
        getUserVotos(user_id, access)
        getUserInformation(user_id, access)
      } else {
        signOut()
      }
    })
  }

  const onRefreshList = () => {
    setRefreshIcon(!true)
    getUseAuth()
  }

  useEffect(() => {
    getUseAuth()
  },[])


  const styles = StyleSheet.create({
    container: {
      flex: 1
    },
    contentContainer: {
      paddingHorizontal: 8,
      paddingVertical: 4,
    },
    item: {
      marginVertical: 4,
    },
  });

  const renderItemHeader = (headerProps, info) => (
    <View {...headerProps} key={info.index}>
      <Text category='h6'>
      {info.item.nome}
      </Text>
      <Text>
       {info.item.descricao}
      </Text>
    </View>
  );

  const renderItemFooter = (footerProps, info) => (
    <Layout {...footerProps}>
       <Text>
        {info.item.votos_positivos} Votos positivos
      </Text>
      <Text>
        {info.item.votos_negativos} Votos negativos
      </Text>
    </Layout>
  );

  const renderItem = (info) => (
    <Card
      key={info.item.id.toString()}
      style={styles.item}
      status='basic'
      header={headerProps => renderItemHeader(headerProps, info)}
      footer={footerProps => renderItemFooter(footerProps, info)}>
      <Image source={{ uri: info.item.gif_url }} style={{width: 270, height: 300}} />
    </Card>
  );

  return (
    <>
    <Header />
    <Layout style={{ height: 100, flexDirection: 'row' }}>
      <Layout style={{ flex: 1, flexDirection: 'row'}}>
        <Layout style={{flex:1,backgroundColor: '#694fad', alignItems: 'center', justifyContent: 'center'}}>
            { userInfo.id == 1 ? <Avatar size='giant' source={require('../assets/avatar.png')}/> :
            <Avatar size='giant' source={{ uri:'https://api.adorable.io/avatars/285/' + userInfo.email}}/> }
        </Layout>
        <Layout style={{ backgroundColor: '#694fad', justifyContent: 'center', flex: 3}}>
          <Text style={{color: 'white',left: 2, textAlign: 'left', fontWeight: 'bold', fontSize:20 }}>{userInfo.username}</Text>
          <Text style={{color: 'white'}}>{userInfo.email}</Text>
        </Layout>
      </Layout>
      <Layout style={{ alignItems: 'center',backgroundColor: '#694fad', justifyContent: 'center' }}>
        <TouchableHighlight onPress={signOut} style={{right: 10}} underlayColor="transparent">
          <MaterialIcons style={{right: 1}} name='exit-to-app' size={30} color='white' />
        </TouchableHighlight>
      </Layout>
    </Layout>
    <Layout style={{ padding: 15 }}>
      <Text style={{ color: '#4b4b4b', fontWeight: 'bold', fontSize: 20, textAlign:'center' }}>Gifs votados por você</Text>
      <Text style={{ color: '#4b4b4b', textAlign:'center', fontSize: 15 }}>Confira novamente puxando para baixo ou mais tarde, para poder exibir os gifs votados por você</Text>
    </Layout>
     <FlatList
      contentContainerStyle={{ alignSelf: 'stretch' }}
      ref={ref}
      onRefresh={onRefreshList}
      refreshing={refreshIcon}
      data={userVotosGifs}
      renderItem={renderItem}
      keyExtractor={item => item.id.toString()}
    /> 
    </>
  );
}