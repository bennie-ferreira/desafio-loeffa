import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import * as eva from '@eva-design/eva';
import { EvaIconsPack } from '@ui-kitten/eva-icons';
import { ApplicationProvider, IconRegistry } from '@ui-kitten/components';
import RouterAuth from './navigation/RouterAuth'
import { AuthProvider } from './context/auth'

export default function App() {
    return (
      <>
      <IconRegistry icons={EvaIconsPack}/>
      <ApplicationProvider {...eva} theme={eva.light}>
        <AuthProvider>
          <NavigationContainer>
            <RouterAuth />
          </NavigationContainer>
        </AuthProvider>
      </ApplicationProvider>
      </>
   );
}