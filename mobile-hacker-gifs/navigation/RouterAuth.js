import React from 'react';

import RouterDefault from './RouterDefault'
import LoginScreen from '../screens/LoginScreen'
import { useAuthContext } from '../context/auth'

export default function RouterAuth() {
  const { signed } = useAuthContext();
 return (
  signed ? (<RouterDefault />) : (<LoginScreen />)
 ) 

}