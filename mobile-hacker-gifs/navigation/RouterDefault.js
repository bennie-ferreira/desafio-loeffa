import React from 'react';
import { createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs';
import VoteScreen from '../screens/VoteScreen'
import UserScreen from '../screens/UserScreen'
import { MaterialIcons, MaterialCommunityIcons } from '@expo/vector-icons';

const Tab = createMaterialBottomTabNavigator();

export default function RouterDefault() {  

    return (
      <Tab.Navigator barStyle={{ backgroundColor: '#694fad' }} inactiveColor='#694fad'>
        <Tab.Screen initialRouteName options={{ tabBarLabel: 'Meu Usuário', tabBarIcon: () => 
        (<MaterialIcons name='account-circle' size={26} color='white' />)}} name="User" component={UserScreen} />

        <Tab.Screen options={{ tabBarLabel: 'Votações', tabBarIcon: () => 
        (<MaterialCommunityIcons  name='vote' size={26} color='white' />)}} name="Votacoes" component={VoteScreen} />

      </Tab.Navigator>
   );
}