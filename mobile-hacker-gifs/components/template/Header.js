import React from 'react'
import { View, StatusBar } from 'react-native';

export default function Header() {

    return (
    <>
        <View style={{ backgroundColor: '#694fad', height: Platform.OS === 'ios' ? 20 : StatusBar.currentHeight}}>
            <StatusBar barStyle="light-content"/>
        </View>
    </>
    );
  }