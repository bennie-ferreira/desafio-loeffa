# Mobile-hacker-gifs

Tecnologias usadas e necessarias para roda o App mobile

  - [NPM]
  - [Node]
  - [Expo cli]
  - [VScode]

# Nota do desenvolvedor

> Apesar do tempo fiquei um pouco doente e não tive tempo
> por causa do meu trabalho, então não deu para
> refatorar o código e deixar mais semântico o possivel 
> mais agradeço a compreenção de todos 
> principalmente na parte do design rsrs..

### Tecnologias usadas nessa aplicação

Aqui estão as tecnologias usadas durante o desenvolvimento da aplicação

* [Expo] - O Expo é uma ferramenta utilizada no desenvolvimento mobile com React Native que permite o fácil acesso às API's nativas do dispositivo sem precisar instalar qualquer dependência ou alterar código nativo
* [React Native] - React Native é uma biblioteca Javascript criada pelo Facebook. É usada para desenvolver aplicativos para os sistemas Android e IOS de forma nativa.

### Instalação

Após fazer o clone da branch master no Git.
Abra o projeto com o editor de código da sua preferência (o meu foi com o VSCODE).
executar os seguintes comandos abaixo:

```sh
$ cd mobile-hacker-gifs
$ npm i
$ npm start
```

For production environments…

```sh
$ cd mobile-hacker-gifs
$ npm i
$ expo build:ios ou expo build:android
```

### Execução do App

 É necessario mudar o endereço do axios para fazer as requisições.
 Fica no diretório services do projeto na seguinte variavel  `baseURL` e colocar o endereço conforme o seu Back End
 ```sh
 mobile-hacker-gifs/services/api.js
```
 

License
----

FREE


**Free Software, Hell Yeah!**

   [Expo]: <https://github.com/joemccann/dillinger>

   [React Native]: <https://github.com/joemccann/dillinger/tree/master/plugins/dropbox/README.md>
   
   [NPM]: <https://www.npmjs.com/>
   
   [Node]: <https://nodejs.org/en/>
   [Expo cli]: <https://expo.io/>
   [VScode]: <https://code.visualstudio.com/>
