import AsyncStorage from '@react-native-community/async-storage';
import api from '../services/api'
import React, { createContext, useContext, useState } from 'react'
import { Alert } from 'react-native'

const Context = createContext();

function AuthProvider({ children }){
    const [user, setUser] = useState(false);

    const signIn = async (values) => {
        if(values.username || values.password){
            await api.post('token/', values)
         .then(response => { 
             const { access } = response.data
             AsyncStorage.setItem('@cgk_api', access);
             setUser(true)
         })
         .catch(error => { 
             const username = error.response.data.username
             const password = error.response.data.password
             const notExist = error.response.data.detail
            if(username || password){
                Alert.alert("Notificação", 'Login Invalido!')
            } else if(notExist != undefined && notExist.includes('No active account found with the given credentials')){
                Alert.alert("Notificação", "Usuário não encontrado.")
            }
         })
        } else {
            Alert.alert("Notificação", "Login e Password não pode está vazio")
        }
    }

    const signOut = async () => { 
        await AsyncStorage.clear()
        setUser(false)
    }

    const useAuth = async () => {
        try {
        let token = await AsyncStorage.getItem('@cgk_api')
         if(token !== null) {
            return token
            }
        } catch(e) {
            return false
      }
    }

    return (
    <Context.Provider value={{signed: !!user, useAuth ,signIn, signOut}}>
        {children}
    </Context.Provider>
    )
}

function useAuthContext() {
    const context = useContext(Context);
  
    if (!context) {
      throw new Error('useAuthContext must be used within an AuthProvider.');
    }
  
    return context;
  }

export { AuthProvider, useAuthContext };