from django.contrib import admin
from django.urls import path, include
from core.views import GifsViewSet, VotosViewSet, UserViewSet, GifsPulibcViewSet
from rest_framework import routers
from rest_framework_simplejwt import views as jwt_views
from django.conf import settings
from django.views.generic import TemplateView

router = routers.DefaultRouter()
router.register('hacker-gifs', GifsViewSet)
router.register('hacker-gifs-public', GifsPulibcViewSet)
router.register('votos', VotosViewSet)
router.register('users', UserViewSet)

urlpatterns = [
    path('', TemplateView.as_view(template_name='index.html')),
    path(r'api/', include(router.urls)),
    path('admin/', admin.site.urls),
    path('api/token/', jwt_views.TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('api/token/refresh/', jwt_views.TokenRefreshView.as_view(), name='token_refresh'),
]