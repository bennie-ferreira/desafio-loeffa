from django.db import models
from django.utils import timezone
from django.conf import settings

class Gif(models.Model):
    gif_url = models.TextField()
    nome = models.CharField(max_length=200)
    descricao = models.TextField()
    votos_positivos = models.IntegerField()
    votos_negativos = models.IntegerField()
    created_date = models.DateTimeField(default=timezone.now)
    published_date = models.DateTimeField(blank=True, null=True)

    def publish(self):
        self.published_date = timezone.now()
        self.save()

    def __str__(self):
        return self.gif_url


class Voto(models.Model):
    gifs_id = models.IntegerField()
    users_id = models.IntegerField()
    users_nome = models.CharField(max_length=200)
    votes = models.CharField(max_length=2)
    created_date = models.DateTimeField(default=timezone.now)
    published_date = models.DateTimeField(blank=True, null=True)

    def publish(self):
        self.published_date = timezone.now()
        self.save()

    def __str__(self):
        return self.users_nome

class User(models.Model):
    first_name = models.CharField(max_length=200)
    username = models.CharField(max_length=200)
    email = models.CharField(max_length=200)

    def __str__(self):
        return self.username