from django.contrib import admin
from .models import Gif, Voto, User

admin.site.register(Gif)
admin.site.register(Voto)