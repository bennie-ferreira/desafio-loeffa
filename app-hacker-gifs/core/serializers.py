from rest_framework import serializers
from .models import Gif, Voto, User
from django.db import models

class GifSerializer(serializers.ModelSerializer):

    class Meta:
        model = Gif
        fields = ['id','gif_url','nome','descricao','votos_positivos','votos_negativos']

class VotoSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = Voto
        fields = ['id','users_id','users_nome','gifs_id','votes']

class UserSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = ['id','username','first_name','email']
