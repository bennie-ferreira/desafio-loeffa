from django.shortcuts import render
from rest_framework import viewsets, mixins
from .models import Gif, Voto, User
from .serializers import GifSerializer, VotoSerializer, UserSerializer
from rest_framework.permissions import IsAuthenticated
from django.db import models

class GifsViewSet(viewsets.ModelViewSet):
    permission_classes = (IsAuthenticated,)

    queryset = Gif.objects.all().order_by("-votos_positivos")
    serializer_class = GifSerializer

class GifsPulibcViewSet(mixins.ListModelMixin, viewsets.GenericViewSet):

    queryset = Gif.objects.all().order_by("-votos_positivos")
    serializer_class = GifSerializer

class VotosViewSet(viewsets.ModelViewSet):
    permission_classes = (IsAuthenticated,)

    queryset = Voto.objects.all()
    serializer_class = VotoSerializer

class UserViewSet(mixins.ListModelMixin, viewsets.GenericViewSet):
    permission_classes = (IsAuthenticated,)

    queryset = User.objects.raw('select * from auth_user')
    serializer_class = UserSerializer
