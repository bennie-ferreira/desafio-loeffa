# App-hacker-gifs

Tecnologias usadas e necessarias para roda o Website e Back End

  - [Python 3]
  - [pip]
  - [VScode] (opcional)
  - [Venv]
  - [Django]

# Nota do desenvolvedor


> Na empresa aonde eu trabalho nunca tinha usado Python para
> Back End, apenas para testes automatizados. 
> Então eu tive uma dificuldade de criar relacionamento entre modelos se eu tivesse
> mais familiaridade com a ferramente teria sido tranquilo para min.

### Tecnologias usadas nessa aplicação

Aqui estão as tecnologias usadas durante o desenvolvimento da aplicação

* [Python 3] - 
* [Django rest framework] - 
* [pip]

### Instalação

Após fazer o clone da branch master no Git.
Abra o projeto com o editor de código da sua preferência (o meu foi com o VSCODE).
executar os seguintes comandos abaixo:
obs: toda a configuração foi feita no sistema operacional linux

```sh
# Para criar um ambiente virtual em Python (Sistema operacional Linux)
$ python3 -m venv myenv
# Para ativar o ambiente virtual Python (Sistema operacional Linux)
$ source myenv/bin/activate
# Para instalar o django
$ pip install django
# Para instalar o django rest framework
$ pip install djangorestframework
# Para install o jwt no django rest framework
$ pip djangorestframework_simplejwt
# entre no diretorio do seu projeto 
$ cd ./app-hacker-gifs
# Para iniciar o servidor (antes de roda coloque o seu IP de rede, não coloque 127.0.0.1)
$ python manage.py runserver 0.0.0.0:8000
```

### Execução do App
Toda aplicação utiliza o banco de dados [SQLIte3] e por esse motivo o banco está populado.
```
# Para acessar o painel administrativo do django admin
http://0.0.0.0:800/admin

# Para listar todas as API do Back End, maioria precisa de autenticação do JWT
http://0.0.0.0:800/api
```

### Migrations e Seeds (Opcional)
Caso você queira instalar do Zero toda aplicação
Delete o arquivo "db.sqlite3" e execute os seguintes comandos
```
# Para criar o usuário admin
$ python manage.py createsuperuser
# para criar migration
$ python manage.py makemigrations
# Para executar as migrations
$ python manage.py migrate
# Para mostrar todas as migrations da aplicação
python manage.py showmigrations
# Para executar seeds
python manage.py loaddata fixtures.json
# Para iniciar o servidor
python manage.py runserver 0.0.0.0:8000
```
 

License
----

FREE


**Free Software, Hell Yeah!**

   [VScode]: <https://code.visualstudio.com/>
   [Python 3]: <https://www.python.org/download/releases/3.0/>
   [pip]: <https://pypi.org/project/pip/>
   [Venv]: <https://docs.python.org/3/library/venv.html>
   [Django]: <https://www.django-rest-framework.org/>
   [Django rest framework]: <https://www.django-rest-framework.org/>
   [SQLIte3]: <https://www.sqlite.org/index.html>
   
